<?php

require('animal.php');

$object = new animal('shaun');

echo "Name Animal : $object->name <br>";
echo "Legs : $object->legs <br>";
echo "Cold Blooded : $object->cold_blooded <br> <br>";

$object2 = new animal('buduk');

echo "Name Animal : $object2->name <br>";
echo "Legs : $object2->legs1 <br>";
echo "Cold Blooded : $object2->cold_blooded <br>";
echo "Jump : $object2->jump <br> <br>";


$object3 = new animal('sungokong');

echo "Name Animal : $object3->name <br>";
echo "Legs : $object3->legs <br>";
echo "Cold Blooded : $object3->cold_blooded <br>";
echo "Yell : $object3->yell <br>";
