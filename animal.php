<?php

    class animal
    {
        public $name;
        public $legs = 4;
        public $legs1 = 2;
        public $cold_blooded = "no";
        public $yell = "Auoooo";
        public $jump = "Hop Hop";

        public function __construct($string)
        {
            $this->name = $string;
        }
    }